package com.lduparc.creditcard.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.util.DebugUtils;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lduparc.creditcard.R;
import com.lduparc.creditcard.fields.CreditCardText;
import com.lduparc.creditcard.fields.CreditEntryFieldBase;
import com.lduparc.creditcard.fields.ExpDateText;
import com.lduparc.creditcard.fields.SecurityCodeText;
import com.lduparc.creditcard.fields.ZipCodeText;
import com.lduparc.creditcard.internal.CreditCardUtil.CardType;
import com.lduparc.creditcard.internal.CreditCardUtil.CreditCardFieldDelegate;
import com.lduparc.creditcard.library.CreditCard;

public class CreditCardEntry extends HorizontalScrollView implements
        OnTouchListener, OnGestureListener, CreditCardFieldDelegate {

    private Context context;

    private LinearLayout container;
    private CreditCardFieldDelegate listener;
    private ImageView cardImage;
    private ImageView backCardImage;
    private CreditCardText creditCardText;
    private ExpDateText expDateText;
    private SecurityCodeText securityCodeText;
    private ZipCodeText zipCodeText;

    private TextView textFourDigits;

    private TextView textHelper;

    private boolean showingBack;

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public CreditCardEntry(Context context) {
        super(context);

        this.context = context;

        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        int width, height;

        if (currentapiVersion < 13) {
            width = display.getWidth(); // deprecated
            height = display.getHeight();
        } else {
            Point size = new Point();
            display.getSize(size);
            width = size.x;
            height = size.y;
        }

        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        setLayoutParams(params);

        this.setHorizontalScrollBarEnabled(false);
        this.setOnTouchListener(this);
        this.setSmoothScrollingEnabled(true);

        container = new LinearLayout(context);
        container.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));
        container.setOrientation(LinearLayout.HORIZONTAL);

        creditCardText = new CreditCardText(context);
        creditCardText.setDelegate(this);
//        creditCardText.setWidth((int) (width));
        container.addView(creditCardText);

        textFourDigits = new TextView(context);
        textFourDigits.setTextSize(20);
//        textFourDigits.setGravity(Gravity.LEFT);
//        textFourDigits.setWidth((int) (width));
        textFourDigits.setVisibility(View.GONE);
        container.addView(textFourDigits);

        expDateText = new ExpDateText(context);
        expDateText.setDelegate(this);
        expDateText.setVisibility(View.GONE);
        container.addView(expDateText);

        securityCodeText = new SecurityCodeText(context);
        securityCodeText.setDelegate(this);
        securityCodeText.setVisibility(View.GONE);
        container.addView(securityCodeText);

        zipCodeText = new ZipCodeText(context);
        zipCodeText.setDelegate(this);
        container.addView(zipCodeText);

        this.addView(container);

        creditCardText.requestFocus();
    }

    public void setEventListener(CreditCardFieldDelegate listener) {
        this.listener = listener;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        focusOnField(creditCardText);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                           float velocityY) {
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public void onCardTypeChange(CardType type) {
        cardImage.setImageResource(CreditCardUtil.cardImageForCardType(type,
                false));
        backCardImage.setImageResource(CreditCardUtil.cardImageForCardType(
                type, true));
        updateCardImage(false);
    }

    @Override
    public void onCreditCardNumberValid() {
        if (listener != null) listener.onCreditCardNumberValid();
        focusOnField(expDateText);

        String number = creditCardText.getText().toString();
        int length = number.length();
        String digits = number.substring(length - 4);
        textFourDigits.setText("  " + digits + "             ");
        textFourDigits.setVisibility(VISIBLE);
        if (expDateText != null)
            expDateText.setVisibility(View.VISIBLE);
        if (securityCodeText != null)
            securityCodeText.setVisibility(View.VISIBLE);
        Log.i("CreditCardNumber", number);
    }

    @Override
    public void onExpirationDateValid() {
        if (listener != null) listener.onExpirationDateValid();
        focusOnField(securityCodeText);
    }

    @Override
    public void onSecurityCodeValid() {
        if (listener != null) listener.onSecurityCodeValid();
        if (zipCodeText != null && zipCodeText.isEnabled())
            focusOnField(zipCodeText);
    }

    @Override
    public void onZipCodeValid() {
        if (listener != null) listener.onZipCodeValid();
    }

    @Override
    public void onBadInput(final EditText field) {
        if (listener != null) listener.onBadInput(field);
        Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);
        field.startAnimation(shake);
        field.setTextColor(Color.RED);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                field.setTextColor(Color.BLACK);
            }
        }, 1000);
    }

    public void setCardImageView(ImageView image) {
        cardImage = image;
    }

    public void activeZipCodeValidation(boolean active) {
        if (zipCodeText != null) {
            zipCodeText.setEnabled(active);
            zipCodeText.setVisibility(active ? VISIBLE : GONE);
        }
    }

    public void updateCardImage(boolean back) {
        if (showingBack != back) {
            flipCardImage();
        }

        showingBack = back;
    }

    public void flipCardImage() {
        FlipAnimator animator = new FlipAnimator(cardImage, backCardImage,
                backCardImage.getWidth() / 2, backCardImage.getHeight() / 2);
        if (cardImage.getVisibility() == View.GONE) {
            animator.reverse();
        }
        cardImage.startAnimation(animator);
    }

    @Override
    public void focusOnField(CreditEntryFieldBase field) {
        if (listener != null) listener.focusOnField(field);
        field.setFocusableInTouchMode(true);
        field.requestFocus();
        field.setFocusableInTouchMode(false);

        if (this.textHelper != null) {
            this.textHelper.setText(field.helperText());
        }

        if (field.getClass().equals(CreditCardText.class)) {
            if (textFourDigits != null)
                textFourDigits.setVisibility(View.GONE);
            if (creditCardText != null)
                creditCardText.setVisibility(View.VISIBLE);
            if (expDateText != null)
                expDateText.setVisibility(View.GONE);
            if (securityCodeText != null)
                securityCodeText.setVisibility(View.GONE);
//            new CountDownTimer(1000, 20) {
//
//                public void onTick(long millisUntilFinished) {
//                    CreditCardEntry.this.scrollTo((int) (millisUntilFinished),
//                            0);
//                }
//
//                public void onFinish() {
//                    CreditCardEntry.this.scrollTo(0, 0);
//                }
//            }.start();
        } else {
            if (textFourDigits != null)
                textFourDigits.setVisibility(View.VISIBLE);
            if (creditCardText != null)
                creditCardText.setVisibility(View.GONE);
            if (expDateText != null)
                expDateText.setVisibility(View.VISIBLE);
            if (securityCodeText != null)
                securityCodeText.setVisibility(View.VISIBLE);
//            new CountDownTimer(1500, 20) {
//
//                public void onTick(long millisUntilFinished) {
//                    CreditCardEntry.this.scrollTo(
//                            (int) (2000 - millisUntilFinished), 0);
//                }
//
//                public void onFinish() {
//
//                }
//            }.start();
        }

        if (field.getClass().equals(SecurityCodeText.class)) {
            ((SecurityCodeText) field).setType(creditCardText.getType());
            updateCardImage(true);
        } else {
            updateCardImage(false);
        }
    }

    @Override
    public void focusOnPreviousField(CreditEntryFieldBase field) {
        if (listener != null) listener.focusOnPreviousField(field);
        if (field.getClass().equals(ExpDateText.class)) {
            focusOnField(creditCardText);
        } else if (field.getClass().equals(SecurityCodeText.class)) {
            focusOnField(expDateText);
        } else if (field.getClass().equals(ZipCodeText.class)) {
            focusOnField(securityCodeText);
        }
    }

    public ImageView getBackCardImage() {
        return backCardImage;
    }

    public void setBackCardImage(ImageView backCardImage) {
        this.backCardImage = backCardImage;
    }

    public TextView getTextHelper() {
        return textHelper;
    }

    public void setTextHelper(TextView textHelper) {
        this.textHelper = textHelper;
    }

    public boolean isCreditCardValid() {
        return creditCardText.isValid() && expDateText.isValid()
                && securityCodeText.isValid() && (zipCodeText == null || !zipCodeText.isEnabled() || zipCodeText.isValid());
    }

    public CreditCard getCreditCard() {
        if (isCreditCardValid()) {
            return new CreditCard(creditCardText.getText().toString(),
                    expDateText.getText().toString(), securityCodeText
                    .getText().toString(), zipCodeText != null && zipCodeText.isEnabled() ? zipCodeText.getText().toString() : ""
            );
        } else {
            return null;
        }
    }

    public void setCardContent(String code, int exp_month, int exp_year, String cvc) {
        textFourDigits.setText(code);
        expDateText.setText(String.format("%02d/%02d", exp_month, (exp_year % 100)));
        securityCodeText.setText("");

        focusOnField(securityCodeText);

        securityCodeText.setFocusableInTouchMode(true);
        securityCodeText.requestFocus();

        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(securityCodeText, InputMethodManager.SHOW_FORCED);

    }
}
